export default class Counter {
    constructor(audio, indicator, event, callback=() => {}, element=window) {
        this.audio = audio
        this.indicator = indicator
        this.element = element
        this.event = event
        this.callback = callback
        this.indexes = []
        this.element.addEventListener(this.event, this.addIndex)
    }

    addIndex = () => {
        if (!this.audio.paused && this.audio.currentTime > 1/1000) {
            this.indexes.push(this.audio.currentTime*1000)
            this.indicator.canvas.classList.add('pulse-border')
            this.indicator.canvas.addEventListener("animationend", () => {
                this.indicator.canvas.classList.remove('pulse-border')
            }, false)
            this.callback()
        }
    }

    stop = () => {
        this.element.removeEventListener(this.event, this.addIndex)
    }
}
