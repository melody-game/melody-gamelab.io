export default class Indicator {
    constructor(audio, canvas, indexes, stick, bar, showTime=800) { //time [ms/canvas]
        this.audio = audio
        this.canvas = canvas
        this.ctx = this.canvas.getContext('2d')
        this.indexes = indexes
        this.stick = stick
        this.bar = bar
        this.stickWidth = this.stick.width
        this.showTime = showTime
        this.speed = 0
        this.center = [this.canvas.width/2, this.canvas.height/2]
        this.resizeCanvas()
        window.addEventListener('resize', this.resizeCanvas)
        this.audio.addEventListener('play', this.start)
    }

    clearCanvas = () => {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        let barHeight = this.canvas.height*0.6
        let barWidth = barHeight * this.bar.width / this.bar.height
        let x = this.center[0]-barWidth/2
        let y = this.center[1]-barHeight/2
        this.ctx.drawImage(this.bar, x, y, barWidth, barHeight)
    }

    resizeCanvas = () => {
        this.canvas.width = window.innerWidth*0.8
        this.canvas.height = 100
        this.center = [this.canvas.width/2, this.canvas.height/2]
        this.stickWidth = this.canvas.height * this.stick.width / this.stick.height
        this.speed = this.center[0]/this.showTime
        this.clearCanvas()
    }

    start = () => requestAnimationFrame(this.update)

    update = () => {
        this.clearCanvas()
        for (let t of this.indexes) {
            let timeDelta = t-this.audio.currentTime*1000
            if (timeDelta >= 0 && timeDelta <= this.center[0]/this.speed) {
                let x = this.center[0]-timeDelta*this.speed-this.stickWidth/2
                let x2 = this.center[0]+timeDelta*this.speed-this.stickWidth/2
                this.ctx.drawImage(this.stick, x, 0, this.stickWidth, this.canvas.height)
                this.ctx.drawImage(this.stick, x2, 0, this.stickWidth, this.canvas.height)
            }
        }
        if (!this.audio.paused) {
            requestAnimationFrame(this.update)
        }
    }
}
