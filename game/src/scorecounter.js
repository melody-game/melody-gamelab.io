function findNearest(indexes, i) {
    return indexes.reduce((a, c) => Math.abs(i-c) < Math.abs(i-a) ? c : a)
}

export default function countScore(audio, indexes, clickIndexes) {
    if (clickIndexes.length === 0) return 0
    const maxInterval = 10000
    let mistakes = 1
    let currentLength = indexes.indexOf(findNearest(indexes, audio.currentTime*1000))
    for (let i = 0; i < currentLength; i++) {
        let range = [indexes[0]-maxInterval, indexes.at(-1)+maxInterval]
        if (i > 0) range[0] = Math.max((indexes[i-1]+indexes[i])/2, indexes[i]-maxInterval)
        if (i < currentLength) range[1] = Math.min((indexes[i+1]+indexes[i])/2, indexes[i]+maxInterval)
        let inRange = 0
        for (let ct of clickIndexes) {
            if (ct >= range[0] && ct < range[1]) {
                inRange++
                mistakes += Math.abs(indexes[i]-ct)
            }
        }
        if (inRange === 0) {
            mistakes += range[1]-range[0]
        }
    }
    mistakes /= currentLength
    return 10**6/mistakes
}
