const MIN_BEAT_TIME = 100
const SUGGEST_TEMPO = [75, 150]

function beatDetectWorker(songData, data, sampleRate) {
    let sr = sampleRate / 1000
    let threshold = songData['threshold']
    let peaksArray = []
    let prevFrames = []
    let change = 0
    for (var i = songData['prev_frames']-1; i < data.length;) {
        prevFrames = data.slice(i-songData['prev_frames']+1, i)
        change = data[i]-Math.min.apply(Math, prevFrames)
        if (change > threshold) {
            peaksArray.push(i / sr)
            i += MIN_BEAT_TIME*sr
        }
        i++
    }
    return peaksArray
}

function getBPM(indexes) {
    let tempos = []
    for (let i = 0; i < indexes.length; i++) {
        let time = 0
        if (i === 0) {
            time = indexes[i]
        } else {
            time = indexes[i]-indexes[i-1]
        }
        let tempo = 60000/time
        while (tempo < SUGGEST_TEMPO[0]) tempo *= 2
        while (tempo > SUGGEST_TEMPO[1]) tempo /= 2
        tempos.push(tempo)
    }
    let roundTempos = tempos.map(a => Math.round(a*100)/100)
    let modeTempo = mode(roundTempos)
    let finalTempo = modeTempo
    if (Math.abs(finalTempo-Math.round(finalTempo)) > 0.01) {
        let candidates = []
        for (let t of tempos) {
            if (t >= finalTempo-2 && t <= finalTempo+2) {
                candidates.push(t)
            }
        }
        finalTempo = average(candidates)
    }
    let tempoIndex = roundTempos.indexOf(modeTempo)
    return [finalTempo, indexes[tempoIndex] % (60000/finalTempo)]
}

function mode(a) {
    return Object.values(
        a.reduce((count, e) => {
            if (!(e in count)) {
                count[e] = [0, e];
            }
            count[e][0]++
            return count
        }, {})
    ).reduce((a, v) => v[0] < a[0] ? a : v, [0, null])[1]
}

function average(l) {
    return l.reduce((a, b) => a + b)/l.length
}


let audContext = new AudioContext()

export default function beatworker(url, songData, callback) {

    function filterAudio(buffer, freq) {
        let newContext = new OfflineAudioContext(1, buffer.length, buffer.sampleRate)
        let source = newContext.createBufferSource()
        source.buffer = buffer
        let merger = new ChannelMergerNode(newContext)
        let filter = new BiquadFilterNode(newContext)
        filter.type = "lowshelf"
        filter.frequency.value = freq
        filter.gain.value = 60
        source.connect(merger)
        merger.connect(filter)
        filter.connect(newContext.destination)
        source.start(0)
        return newContext.startRendering()
    }
    document.getElementById('loading-modal').style.display = 'block'
    document.getElementById('loading').innerText = 'Detecting beats...'
    var request = new XMLHttpRequest()
    request.open('GET', url, true)
    request.responseType = 'arraybuffer'
    request.onload = function (e) {
        audContext.decodeAudioData(e.target.response, (buffer) => {
            filterAudio(buffer, songData['frequency']).then((buffer) => {
                document.getElementById('loading').innerText = 'Almost there...'
                let indexes = beatDetectWorker(songData, buffer.getChannelData(0), buffer.sampleRate)
                let bpmData = getBPM(indexes)
                let offset = bpmData[1]
                let bpm = bpmData[0]
                let bpmIndexes = []
                for (let t = offset; t <= buffer.duration*1000; t += 60000/bpm) {
                    for (let i = 0; i < indexes.length; i++) {
                        let ct = indexes[i]
                        let diff = Math.abs(ct-t)
                        let prevDiff = Math.abs(indexes[i-1]-t)
                        let nextDiff = Math.abs(indexes[i+1]-t)
                        if (diff <= 20 || (diff <= 100 && prevDiff >= 120 && nextDiff >= 120)) {
                            t = ct
                        }
                    }
                    bpmIndexes.push(t)
                }
                document.getElementById('loading').innerText = 'Done!'
                callback(indexes, bpmIndexes)
            })
        }).catch(() => {
            document.getElementById('loading-modal').style.display = 'none'
            document.getElementById('error-modal').style.display = 'block'
        })
    }
    request.send()
}
