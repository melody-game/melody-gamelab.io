import beatworker from "./mybeatworker.js"
import Indicator from "./indicator.js"
import Counter from "./counter.js"
import Accel from './accelerometer.js'
import countScore from "./scorecounter.js"

const songDataSrc = './music/songs.json'
const audioDir = './music/'
const barImage = new Image()
barImage.src = './images/bar.png'
const stickImage = new Image()
stickImage.src = './images/stick.png'

const body = document.getElementsByTagName('body')[0]
const pauseButton = document.getElementById('pause')
const scoreLabel = document.getElementById('score')
const indicatorCan = document.getElementById('click-indicator')
const bpmIndicatorCan = document.getElementById('bpm-indicator')
const musicInput = document.getElementById('music-input')
const playButton = document.getElementById('play-button')
const leftKeys = [192, 49, 50, 51, 52, 53, 81, 87, 69, 82, 84, 65, 83, 68, 70, 71, 90, 88, 67, 86, 32]
const rightKeys = [54, 55, 56, 57, 48, 189, 187, 89, 85, 73, 79, 80, 219, 221, 220, 74, 75, 76, 186, 222, 66, 78, 77, 188, 190, 191]

const version = '1.2'
const beatDataKey = 'beat-data-2'
const maxScoresKey = 'max-scores-2'

function start(audioSrc, audioFile) {
    playButton.style.display = 'none'
    document.getElementById('loading-spinner').style.display = 'block'
    document.getElementById('loading-modal').style.display = 'none'
    aborted = false
    let beatData = {}
    if (localStorage.getItem(beatDataKey) === null) {
        localStorage.setItem(beatDataKey, JSON.stringify({}))
    } else {
        beatData = JSON.parse(localStorage.getItem(beatDataKey))
    }
    audio.src = audioSrc
    audio.load()

    function hideModals() {
        Array.from(document.querySelectorAll('.my-modal')).forEach(modal => {
            modal.style.display = 'none'
        })
    }

    function revealPlay(indexes, bpmIndexes) {
        document.getElementById('loading-spinner').style.display = 'none'
        playButton.style.display = 'block'
        playButton.onclick = () => playGame(indexes, bpmIndexes)
    }

    let curSongData = {
        'name': audioFile,
        'author': '',
        'threshold': document.getElementById('thres-select').value,
        'frequency': document.getElementById('freq-select').value,
        'prev_frames': document.getElementById('frames-select').value
    }
    if (audioFile in beatData) {
        curSongData = songData[audioFile]
        playGame(...beatData[audioFile])
    } else if (audioFile in songData) {
        curSongData = songData[audioFile]
        beatworker(audioSrc, curSongData, revealPlay)
    } else {
        document.getElementById('settings-modal').style.display = 'block'
        document.title = `${audioFile} | MELODY`
        document.getElementById('apply-settings').onclick = () => {
            document.getElementById('settings-modal').style.display = 'none'
            beatworker(audioSrc, curSongData, revealPlay)
        }
    }
    document.title = `${curSongData['name']} | MELODY`

    function playGame(indexes, bpmIndexes) {
        hideModals()
        switchTab('game')
        if (audioFile in songData) {
            beatData[audioFile] = [indexes, bpmIndexes]
            localStorage.setItem(beatDataKey, JSON.stringify(beatData))
        }
        let bpmIndicator = new Indicator(audio, bpmIndicatorCan, bpmIndexes, stickImage, barImage)
        let clickIndicator = new Indicator(audio, indicatorCan, indexes, stickImage, barImage)
        let bpmCounter = new Counter(audio, bpmIndicator, 'shake', updateScore)
        let clickCounter = new Counter(audio, clickIndicator, 'beatclick', updateScore)
        let updateInterval = setInterval(updateScore, 500)
        let progressInterval = setInterval(() => {
            document.getElementById('time-progress').style.width = `${audio.currentTime/audio.duration*100}%`
        }, 1000)

        function updateScore() {
            clickScore = countScore(audio, indexes, clickCounter.indexes)
            bpmScore = countScore(audio, bpmIndexes, bpmCounter.indexes)
            score = Math.round((clickScore+bpmScore)/2)
            scoreLabel.innerText = score.toLocaleString('en-US')
        }

        pauseButton.onclick = (e) => {
            e.stopPropagation()
            audio.pause()
        }
        document.getElementById('p-resume-btn').onclick = () => {
            audio.play()
        }

        Array.from(document.querySelectorAll('.home-btn')).forEach(btn => btn.onclick = (e) => {
            aborted = true
            hideModals()
            e.stopPropagation()
            bpmScore = 0
            clickScore = 0
            clickCounter.stop()
            bpmCounter.stop()
            document.getElementById('time-progress').style.width = 0
            clearInterval(updateInterval)
            clearInterval(progressInterval)
            audio.pause()
            audio.currentTime = 0
            document.title = 'MELODY'
            switchTab('menu')
        })

        audio.onplay = () => {
            document.getElementById('pause-modal').style.display = 'none'
        }
        audio.onpause = () => {
            if (audio.ended) {
                let levelName = 'custom'
                if (audioFile in songData) {
                    levelName = audioFile
                    if (!(audioFile in maxScores) || score > maxScores[audioFile]) {
                        maxScores[audioFile] = score
                        localStorage.setItem(maxScoresKey, JSON.stringify(maxScores))
                        generateMenu()
                    }
                }
                gtag('event', 'level_end', {
                    'level_name': levelName,
                    'score': score
                })
                updateScore()
                document.getElementById('final-score').innerText = score.toLocaleString('en-US')
                document.getElementById('end-modal').style.display = 'block'
            } else if (!aborted) {
                document.getElementById('pause-modal').style.display = 'block'
            }
        }
        audio.play()
    }
}

function switchTab(tab) {
    Array.from(document.getElementsByClassName('tab')).forEach(t => t.style.display = 'none')
    document.getElementById(tab).style.display = 'block'
}

function generateMenu() {
    if (localStorage.getItem(maxScoresKey) === null) {
        localStorage.setItem(maxScoresKey, JSON.stringify({}))
    } else {
        maxScores = JSON.parse(localStorage.getItem(maxScoresKey))
    }
    document.getElementById('song-container').innerText = ''
    for (let file of Object.keys(songData).sort((k1, k2) => {
        return songData[k1]['difficulty']-songData[k2]['difficulty']
    })) {
        let card = document.createElement('div')
        card.addEventListener('click', () => {
            start(audioDir+file, file)
            gtag('event', 'level_start', {
                'level_name': file
            })
        })
        card.classList.add('card', 'music-link')
        let nameContainer = document.createElement('h5')
        nameContainer.innerText = songData[file]['name']
        nameContainer.classList.add('music-name')
        card.appendChild(nameContainer)
        let authorContainer = document.createElement('div')
        authorContainer.innerText = songData[file]['author']
        authorContainer.classList.add('music-author')
        card.appendChild(authorContainer)
        if (file in maxScores) {
            let scoreContainer = document.createElement('div')
            scoreContainer.innerText = `Max score: ${maxScores[file].toLocaleString('en-US')}`
            scoreContainer.classList.add('music-score')
            card.appendChild(scoreContainer)
        }
        document.getElementById('song-container').appendChild(card)
    }
}

let bpmScore = 0
let clickScore = 0
let score = 0
let aborted = false
let songData = {}
let maxScores = {}
let audio = new Audio()
let dataRequest = new XMLHttpRequest()
dataRequest.open('GET', songDataSrc)
dataRequest.responseType = 'json'
dataRequest.send()
dataRequest.onload = () => {
    songData = dataRequest.response
    let accel = new Accel(() => {
        document.getElementById('accel-support').style.display = 'block'
    })
    if (localStorage.getItem('help-showed') === null) {
        localStorage.setItem('help-showed', JSON.stringify(true))
        document.getElementById('help-modal').style.display = 'block'
    }
    let lastVersion = localStorage.getItem('chlog-last-version')
    if (lastVersion === null || JSON.parse(lastVersion) !== version) {
        localStorage.setItem('chlog-last-version', JSON.stringify(version))
        document.getElementById('changelog-modal').style.display = 'block'
    }

    document.getElementById('play-own-btn').onclick = () => {
        if (musicInput.files.length > 0) {
            start(URL.createObjectURL(musicInput.files[0]), musicInput.files[0].name)
            gtag('event', 'level_start', {
                'level_name': 'custom'
            })
        }
    }
    document.getElementById('help-btn').onclick = () => {
        document.getElementById('help-modal').style.display = 'block'
    }
    document.getElementById('changelog-btn').onclick = () => {
        document.getElementById('changelog-modal').style.display = 'block'
    }
    window.onclick = () => window.dispatchEvent(new Event('beatclick'))
    window.onkeydown = e => {
        if (e.key === 'Escape') {
            if (!audio.paused) {
                audio.pause()
            } else {
                audio.play()
            }
        } else if (e.key === 'h') {
            document.getElementById('help-modal').style.display = 'block'
        } else if (leftKeys.some(a => e.keyCode === a)) {
            window.dispatchEvent(new Event('shake'))
        } else if (rightKeys.some(a => e.keyCode === a)) {
            window.dispatchEvent(new Event('beatclick'))
        }
    }
    generateMenu()
    for (let i = 1; i <= 10; i += 1) {
        let opt = document.createElement('option')
        opt.innerText = i/10
        opt.value = i/10
        if (i === 3) {
            opt.selected = true
        }
        document.getElementById('thres-select').appendChild(opt)
    }
    Array.from(document.querySelectorAll('.my-modal')).forEach(modal => {
        Array.from(modal.querySelectorAll('.dismiss')).forEach(btn => {
            btn.onclick = () => modal.style.display = 'none'
        })
    })
}

switchTab('menu')
