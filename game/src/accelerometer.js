export default class Accel {
    constructor(onError=() => {}) {
        this.wasDown = false
        this.shakeEvent = new Event('shake')
        this.onError = onError
        this.lastMeasured = 0

        if (typeof (DeviceMotionEvent) !== "undefined") {
            window.addEventListener('devicemotion', (e) => {
                this.lastMeasured = Date.now()
                let x = e.accelerationIncludingGravity.x
                let y = e.accelerationIncludingGravity.y
                let z = e.accelerationIncludingGravity.z
                let acc = Math.hypot(Math.hypot(x, y), z)
                if (!this.wasDown && acc > 15) {
                    this.wasDown = true
                } else if (this.wasDown && acc < 5) {
                    this.wasDown = false
                    window.dispatchEvent(this.shakeEvent)
                }
            })
        }
        setTimeout(() => {
            if (Date.now()-this.lastMeasured > 100) {
                this.onError()
            }
        }, 150)
    }
}
